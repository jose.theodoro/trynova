@extends('layout.base')

@section('title', 'Abrir Ordem de Serviço')
@section('page-title', 'Abrir Ordem de Serviço')
@section('page-title-description', 'abertura de os')


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('os.store')}}">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6">
                                    <div class="position-relative form-group">
                                            <label class="">Cliente</label>
                                            <select name="customer_id" class="form-control">
                                                <option value="">Selecione...</option>
                                                @foreach($customers as $customer)
                                                    <option value="{{$customer->id}}">{{$customer->name}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label class="">Unidades</label>
                                    <select name="property_type_id" class="form-control">
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                        <label class="">Serviços</label>
                                        <select name="customer_id" class="form-control">
                                           
                                        </select>
                                </div>
                            </div>
                        </div>
                        <a class="mt-2 btn btn-secondary" href="{{route('dashboard.index')}}">Voltar</a>
                        <button type="submit" class="mt-2 btn btn-alternate" href="#">Abir OS</button>
                    </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="mb-3 card">
            <div class="card-header">Atividades</div>
            <div class="card-body">
                
            </div>
        </div>
    </div>
</div>

@endsection