@extends('layout.base')

@section('title', 'O.S Nro 1234')
@section('page-title', 'Ordem de Serviço')
@section('page-title-description', 'Detalhes de O.S')


@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="mb-3 card">
            <div class="card-header">
                <ul class="nav">
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg-0" class="active nav-link">Geral</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg-1" class="nav-link">Financeiro</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg-2" class="nav-link">Arquivos</a></li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab-eg-0" role="tabpanel">
                        <div class="form-row">
                            <div class="col-md-1">
                                <div class="position-relative form-group"><label for="exampleCity" class=""><b>Número O.S</b></label>
                                <p>STD-1234</p>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="position-relative form-group">
                                    <label for="exampleState" class=""><b>UNIORG</b></label>
                                    <p>12345</p>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="position-relative form-group">
                                    <label for="exampleState" class=""><b>Cliente</b></label>
                                    <p>Santander</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                <label for="exampleState" class=""><b>Unidade</b></label>
                                    <p>Santander Unidade 123</p>
                                    </div>
                            </div>
                            <div class="col-md-2">
                                <div class="position-relative form-group">
                                    <label for="exampleState" class=""><b>Região</b></label>
                                    <p>São Paulo Metropolitana</p>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="position-relative form-group">
                                    <label for="exampleState" class=""><b>Analista</b></label>
                                    <p>Maria dos Santos Alves</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="exampleState" class=""><b>Serviço</b></label>
                                    <p>Licença Ambiental</p>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="position-relative form-group">
                                    <label for="exampleState" class=""><b>Data de Abertura</b></label>
                                    <p>30/12/2019</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="exampleState" class=""><b>Data de Vencimento - SLA</b></label>
                                    <p>30/06/2020</p>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="position-relative form-group">
                                <label for="exampleState" class=""><b>Status</b></label>
                                    <p>Em Andamento</p>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-eg-1" role="tabpanel"></div>
                    <div class="tab-pane" id="tab-eg-2" role="tabpanel"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-header"><i class="header-icon pe-7s-plugin icon-gradient bg-plum-plate"> </i>Atividades</div>
            <div class="card-body">
            <div class="vertical-time-simple vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                                                <div class="vertical-timeline-item dot-primary vertical-timeline-element">
                                                                    <div>
                                                                        <span class="vertical-timeline-element-icon bounce-in"></span>
                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                            <h4 class="timeline-title">Atividade 01 <span class="text-primary">em andamento</span> </h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="vertical-timeline-item dot-success vertical-timeline-element">
                                                                    <div>
                                                                        <span class="vertical-timeline-element-icon bounce-in"></span>
                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                            <h4 class="timeline-title">Atividade 02 <span class="text-success">concluído em 31/08/2019</span>
                                                                            </h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="vertical-timeline-item dot-success vertical-timeline-element">
                                                                    <div>
                                                                        <span class="vertical-timeline-element-icon bounce-in"></span>
                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                            <h4 class="timeline-title">Atividade 03
                                                                                
                                                                            </h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="vertical-timeline-item dot-success vertical-timeline-element">
                                                                    <div>
                                                                        <span class="vertical-timeline-element-icon bounce-in"></span>
                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                            <h4 class="timeline-title">
                                                                                Atividade 05
                                                                            </h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="vertical-timeline-item dot-success vertical-timeline-element">
                                                                    <div>
                                                                        <span class="vertical-timeline-element-icon bounce-in"></span>
                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                            <h4 class="timeline-title">Atividade 06</h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="vertical-timeline-item dot-success vertical-timeline-element">
                                                                    <div>
                                                                        <span class="vertical-timeline-element-icon bounce-in"></span>
                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                            <h4 class="timeline-title">Atividade 07</h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-header"><i class="header-icon pe-7s-chat icon-gradient bg-plum-plate"> </i>Comentários</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pane-left">
                            <div class="avatar-icon-wrapper mr-2">
                                <div class="avatar-icon avatar-icon-xl rounded"><img width="82" src="/lib/ui/images/avatars/1.jpg" alt=""></div>
                            </div>
                            <h6 class="mb-0 text-nowrap">
                                <b>Maria dos Santos Alves</b>
                                <div class="opacity-7">
                                    adicionou um novo comentário em 31/ago/19
                                </div>
                            </h6>
                        </div>
                        <div>
                            <p style="padding: 10px">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lacus sed turpis tincidunt id. Pellentesque eu tincidunt tortor aliquam nulla facilisi cras. Fringilla urna porttitor rhoncus dolor purus. Mattis rhoncus urna neque viverra justo. Libero enim sed faucibus turpis in. Erat pellentesque adipiscing commodo elit at. Sit amet nisl purus in mollis nunc sed id. Urna nunc id cursus metus aliquam eleifend mi in. Platea dictumst vestibulum rhoncus est pellentesque elit. Malesuada bibendum arcu vitae elementum curabitur vitae nunc sed velit. Odio pellentesque diam volutpat commodo sed egestas egestas fringilla. Mollis nunc sed id semper risus in. Eget nulla facilisi etiam dignissim diam. Vel turpis nunc eget lorem dolor. Vel orci porta non pulvinar neque laoreet suspendisse. Eu nisl nunc mi ipsum faucibus vitae aliquet nec.</p>
                        </div>
                        <div class="divider"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="pane-left">
                            <div class="avatar-icon-wrapper mr-2">
                                <div class="avatar-icon avatar-icon-xl rounded"><img width="82" src="/lib/ui/images/avatars/1.jpg" alt=""></div>
                            </div>
                            <h6 class="mb-0 text-nowrap">
                                <b>Maria dos Santos Alves</b>
                                <div class="opacity-7">
                                    adicionou um novo comentário em 25/ago/19
                                </div>
                            </h6>
                        </div>
                        <div>
                            <p style="padding: 10px">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lacus sed turpis tincidunt id. Pellentesque eu tincidunt tortor aliquam nulla facilisi cras. Fringilla urna porttitor rhoncus dolor purus. Mattis rhoncus urna neque viverra justo. Libero enim sed faucibus turpis in. Erat pellentesque adipiscing commodo elit at. Sit amet nisl purus in mollis nunc sed id. Urna nunc id cursus metus aliquam eleifend mi in. Platea dictumst vestibulum rhoncus est pellentesque elit. Malesuada bibendum arcu vitae elementum curabitur vitae nunc sed velit. Odio pellentesque diam volutpat commodo sed egestas egestas fringilla. Mollis nunc sed id semper risus in. Eget nulla facilisi etiam dignissim diam. </p>
                        </div>
                        <div class="divider"></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <textarea placeholder='Escreva aqui para deixar um comentário' rows="1" class="form-control autosize-input" style="height: 50px;"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection