<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Trynova Gestão - Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <link rel="stylesheet" href="/lib/ui/css/base.css">

</head>
<body>
	<div class="app-container app-theme-white body-tabs-shadow">
			<div class="app-container">
				<div class="h-100 unioit-bg-plum-plate bg-animation">
					<div class="d-flex h-100 justify-content-center align-items-center">
						<div class="mx-auto app-login-box col-md-8">
							<!-- <div class="app-logo-inverse mx-auto mb-3"></div> -->
							<div class="modal-dialog w-100 mx-auto">
								<div class="modal-content">
									<div class="modal-body">
										<div class="h5 modal-title text-center">
											<h4 class="mt-2">
												<div><img src="http://www.trynova.com.br/wp-content/uploads/2017/12/www.trynova.com.br-logo-topo.png" alt=""></div>
												<!-- <span>Faça login para continuar.</span> -->
											</h4>
										</div>
										<form class="">
											<div class="form-row">
												<div class="col-md-12">
													<div class="position-relative form-group"><input name="email" placeholder="Email" type="email" class="form-control"></div>
												</div>
												<div class="col-md-12">
													<div class="position-relative form-group"><input name="password" placeholder="Senha" type="password" class="form-control"></div>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="float-left"><a href="javascript:void(0);" class="btn-lg btn btn-link">Esqueci minha senha</a></div>
										<div class="float-right">
											<a href="{{route('dashboard.index')}}" class="btn btn-primary btn-lg">Entrar</a>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="text-center text-white opacity-8 mt-3">Copyright © Trynova 2019</div> -->
						</div>
					</div>
				</div>
			</div>
	</div>

<!--CORE-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</body>
</html>
