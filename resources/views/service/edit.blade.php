@extends('layout.base')

@section('title', 'Serviços')
@section('page-title', 'Serviços')
@section('page-title-description', 'cire uma nova serviço.')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('service.update')}}">
                    @csrf
                    <input name="id" value="{{$service->id}}" type="hidden" class="form-control">
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label class="">Nome do Serviço</label>
                                <input name="name" type="text" class="form-control" value="{{$service->name}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                    <label class="">Cliente</label>
                                    <select name="customer_id" class="form-control">
                                        @foreach($customers as $customer)
                                            <option {{$service->customer_id == $customer->id ? 'selected' : '' }} value="{{$customer->id}}">{{$customer->name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                    <label class="">Região</label>
                                    <select name="region_id" class="form-control">
                                        @foreach($regions as $region)
                                            <option {{$service->region_id == $region->id ? 'selected' : '' }} value="{{$region->id}}">{{$region->name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="position-relative form-group">
                                    <label class="">Código</label>
                                    <input name="custom_id" class="form-control" value="{{$service->custom_id}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="position-relative form-group">
                                    <label class="">Preço</label>
                                    <input name="price" class="form-control" value="{{$service->price}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label class="">Descrição</label>
                                <textarea name="description" cols="10" rows="10" class="form-control">{{$service->description}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label class="">Atividades</label><br>
                                @foreach($tasks as $task)
                                    <div class="custom-checkbox custom-control custom-control-inline">
                                        @foreach($service->tasks as $t)
                                            @if($task->id == $t->id)
                                                <input type="checkbox" id="task_{{$task->id}}" value="{{$task->id}}" name="tasks[{{$task->id}}]" checked class="custom-control-input">
                                                @continue
                                            @endif
                                        @endforeach
                                        <input type="checkbox" id="task_{{$task->id}}" value="{{$task->id}}" name="tasks[{{$task->id}}]" class="custom-control-input">
                                        <label class="custom-control-label" for="task_{{$task->id}}">
                                            {{$task->name}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('service.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection