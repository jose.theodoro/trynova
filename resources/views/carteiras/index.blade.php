@extends('layout.base')

@section('title', 'Carteiras')
@section('page-title', 'Carteiras')
@section('page-title-description', 'Carteiras Cadastradas')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <table style="width: 100%;" id="data-table" class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 30px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Cateira</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 40px;" aria-label="Age: activate to sort column ascending">Gerente Responsável</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 15px;" aria-label="Start date: activate to sort column ascending">Data de Criação</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 5px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Santander</td>
                            <td>Maria dos Santos Alves</td>
                            <td>30/08/19</td>
                            <td class="text-center">
                                <a href="{{route('carteiras.show', 2)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Youcom</td>
                            <td>Maria dos Santos Alves</td>
                            <td>30/09/19</td>
                            <td class="text-center">
                                <a href="#">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection