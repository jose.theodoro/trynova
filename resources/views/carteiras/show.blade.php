@extends('layout.base')

@section('title', 'Carteiras')
@section('page-title', 'Carteiras')
@section('page-title-description', 'mais detalhes sobre a carteira Santander')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
                <div class="card-body">

                <form class="">
                                                                <div class="form-row">
                                                                    <div class="col-md-6">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleEmail11" class="">Carteira</label>
                                                                            <input name="carteira" type="text" class="form-control" value="Satander Brasil">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="position-relative form-group">
                                                                            <label for="examplePassword11" class="">Gerente da Conta</label>
                                                                            <select class="form-control" name="gerente" id="">
                                                                                <option value="1">Maria dos Santos Alves</option>
                                                                                <option value="1">João da Silva</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <div class="col-md-6">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleCity" class="">Mais alguma informação</label>
                                                                            <input name="city" type="text" class="form-control" value="Mais alguma informação...">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleState" class="">Alguma Coisa</label>
                                                                            <input name="state" type="text" class="form-control" value="mais alguma coisa">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleZip" class="">Data de Criação</label>
                                                                            <input name="created_at" disabled="true" type="text" class="form-control" value="30/08/2019">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class="col-md-2"> -->
                                                                <a class="mt-2 btn btn-secondary" href="{{route('carteiras.index')}}">Voltar</a>
                                                                <a class="mt-2 btn btn-alternate" href="#">Salvar</a>
                                                                <!-- </div> -->
                                                            </form>




                </div>
        </div>
    </div>
</div>
@endsection