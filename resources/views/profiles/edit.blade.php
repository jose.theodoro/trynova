@extends('layout.base')

@section('title', 'Perfil')
@section('page-title', 'Perfil')
@section('page-title-description', 'cire uma nova área.')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('profiles.update')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{$profile->id}}">
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label class="">Perfil</label>
                                <input name="name" type="text" class="form-control" value="{{$profile->name}}">
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('profiles.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection