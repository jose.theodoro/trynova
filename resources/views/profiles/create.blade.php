@extends('layout.base')

@section('title', 'Perfis')
@section('page-title', 'Perfil')
@section('page-title-description', 'cire um novo perfil.')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('profiles.store')}}">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label for="profile" class="">Perfil</label>
                                <input id="profile" name="name" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('profiles.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection