@extends('layout.base')

@section('title', 'Perfis')
@section('page-title', 'Perfis')
@section('page-title-description', 'Perfis Cadastrados')


@section('content')
<div class="dropdown d-inline-block">
    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">
        <i class="fa fa-bars"></i> Ações
    </button>
    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-hover-link dropdown-menu">
        <button type="button" tabindex="0" class="dropdown-item">
         <a href="{{route('profiles.create')}}">
         <i class="dropdown-icon fa fa-plus"> </i> Novo Perfil
         </a>
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <table style="width: 100%;" id="data-table" class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 40px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Perfil</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 5px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($profiles as $profile)
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">{{$profile->name}}</td>
                            <td class="text-center">
                                <a href="{{route('profiles.edit', $profile->id)}}">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <a href="javascript:;" onclick="confirmDelete(this)" data-href="{{route('profiles.delete', $profile->id)}}">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>





@endsection
