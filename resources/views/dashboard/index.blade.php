@extends('layout.base')

@section('title', 'Dashboard')
@section('page-title', 'Dashboard')
@section('page-title-description', 'Um resumo do que está acontecendo')


@section('content')
<div class="main-card mb-3 card">
    <div class="card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
        Ordens de Serviço
        </div>
        <div class="btn-actions-pane-right">
            <a class="text-white btn-shadow btn btn-primary" href="{{route('os.create')}}">Abrir Ordem de Serviço</a>
        </div>
    </div>

    <div class="table-responsive">
        <table class="align-middle text-truncate mb-0 table table-borderless table-hover">
            <thead>
            <tr>
            <th class="text-center">Nro O.S</th>
            <th class="text-center">Analista</th>
            <th class="text-center">Cliente</th>
            <th class="text-center">Aberta em</th>
            <th class="text-center">Serviço</th>
            <th class="text-center">Atividades</th>
            <th class="text-center">Status</th>
            <th class="text-center"></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center text-muted" style="width: 80px;">STD-54</td>
                    <td class="text-center"><a href="javascript:void(0)">José</a></td>
                    <td class="text-center"><a href="javascript:void(0)">Santander</a></td>
                    <td class="text-center">07/08/2019</td>
                    <td class="text-center">Alvará</td>
                    <td class="text-center" style="width: 200px;">
                        <div class="widget-content p-0">
                            <div class="widget-content-outer">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left pr-2">
                                        <div class="widget-numbers fsize-1 text-danger">71%</div>
                                    </div>
                                    <div class="widget-content-right w-100">
                                        <div class="progress-bar-xs progress">
                                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100" style="width: 71%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="badge badge-pill badge-danger">Cancelada</div>
                    </td>
                    <td class="text-center">
                        <div role="group" class="btn-group-sm btn-group">
                        <a href="{{route('os.show', 1234)}}" class="btn-shadow btn btn-primary">Visualizar</a>
                        </div>
                    </td>
                </tr>
            <tr>
            <td class="text-center text-muted" style="width: 80px;">You-55</td>
            <td class="text-center"><a href="javascript:void(0)">Maria</a></td>
            <td class="text-center"><a href="javascript:void(0)">Youcom</a>
            </td>
            <td class="text-center">

            15/07/2019
            </td>
            <td class="text-center">Licença Ambiental</td>
            <td class="text-center" style="width: 200px;">
            <div class="widget-content p-0">
            <div class="widget-content-outer">
            <div class="widget-content-wrapper">
            <div class="widget-content-left pr-2">
            <div class="widget-numbers fsize-1 text-warning">
            54%
            </div>
            </div>
            <div class="widget-content-right w-100">
            <div class="progress-bar-xs progress">
            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="54" aria-valuemin="0" aria-valuemax="100" style="width: 54%;"></div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </td>
            <td class="text-center">
            <div class="badge badge-pill badge-info">Em Espera</div>
            </td>
            <td class="text-center">
            <div role="group" class="btn-group-sm btn-group">
            <a href="#" class="btn-shadow btn btn-primary">Visualizar</a>
            </div>
            </td>
            </tr>
            <tr>
            <td class="text-center text-muted" style="width: 80px;">STD-56</td>
            <td class="text-center"><a href="javascript:void(0)">José</a>
            </td>
            <td class="text-center"><a href="javascript:void(0)">Santander</a></td>                                                        
            <td class="text-center">

            20/07/2019
            </td>
            <td class="text-center">AVCB</td>
            <td class="text-center" style="width: 200px;">
            <div class="widget-content p-0">
            <div class="widget-content-outer">
            <div class="widget-content-wrapper">
            <div class="widget-content-left pr-2">
            <div class="widget-numbers fsize-1 text-success">
            97%
            </div>
            </div>
            <div class="widget-content-right w-100">
            <div class="progress-bar-xs progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="97" aria-valuemin="0" aria-valuemax="100" style="width: 97%;"></div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </td>
            <td class="text-center">
            <div class="badge badge-pill badge-warning">Em Andamento</div>
            </td>
            <td class="text-center">
            <div role="group" class="btn-group-sm btn-group">
            <a href="#" class="btn-shadow btn btn-primary">Visualizar</a>
            </div>
            </td>
            </tr>
            <tr>
            <td class="text-center text-muted" style="width: 80px;">RCH-56</td>
            <td class="text-center"><a href="javascript:void(0)">Fulano</a></td>
            <td class="text-center"><a href="javascript:void(0)">Riachuelo</a>
            </td>
            <td class="text-center">
            Hoje
            </td>
            <td class="text-center">Inscrição Estadual</td>
            <td class="text-center" style="width: 200px;">
            <div class="widget-content p-0">
            <div class="widget-content-outer">
            <div class="widget-content-wrapper">
            <div class="widget-content-left pr-2">
            <div class="widget-numbers fsize-1 text-info">
            88%
            </div>
            </div>
            <div class="widget-content-right w-100">
            <div class="progress-bar-xs progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100" style="width: 88%;"></div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </td>
            <td class="text-center">
            <div class="badge badge-pill badge-success">Concluída</div>
            </td>
            <td class="text-center">
            <div role="group" class="btn-group-sm btn-group">
            <a href="#" class="btn-shadow btn btn-primary">Visualizar</a>
            </div>
            </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="d-block p-4 text-center card-footer">

    </div>
</div>
@endsection