@extends('layout.base')

@section('title', 'Regiões')
@section('page-title', 'Região')
@section('page-title-description', 'cire uma nova região.')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('region.update')}}">
                    @csrf
                    <input id="name" name="id" type="hidden" class="form-control" value="{{ $region->id }}">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label>Região</label>
                                <input name="name" type="text" class="form-control" value="{{$region->name}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label class="">Estado</label>
                                <select class="form-control" name="state_id" id="state">
                                    <option value="">Selecione um estado ...</option>
                                    @foreach($states as $state)
                                        <option {{$region->state_id == $state->id ? 'selected' : ''}} value="{{$state->id}}">{{$state->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label class="">Cidades</label><br>
                                <div id="cities-container">
                                @foreach($region->state->cities as $city)
                                <div class="custom-checkbox custom-control custom-control-inline">
                                    @foreach($region->cities as $c)
                                        @if($city->id == $c->id)
                                            <input type="checkbox" id="city_{{$city->id}}" value="{{$city->id}}" name="cities[{{$city->id}}]" checked class="custom-control-input">
                                            @continue
                                        @endif
                                    @endforeach
                                    <input type="checkbox" id="city_{{$city->id}}" value="{{$city->id}}" name="cities[{{$city->id}}]" class="custom-control-input">
                                    <label class="custom-control-label" for="city_{{$city->id}}">
                                        {{$city->name}}
                                    </label>
                                </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('region.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
$(function(){
 
$('#state').change(function() {
    let state_id = $(this).val()
    $.ajax({
        type: 'GET',
        url: `/region/state/${state_id}/cities`,
        success: function(data) {
            let cities = data['data']
            
            let option = '';
           cities.forEach(function(city, key) {
                option += `<div class="custom-checkbox custom-control custom-control-inline">
                            <input type="checkbox" id="cities_${city.id}" value="${city.id}" name="cities[${city.id}]" class="custom-control-input">
                            <label class="custom-control-label" for="cities_${city.id}">
                                ${city.name}
                            </label>
                        </div>`
           })
            $('#cities-container').html(option);
            
        }
    })
})

})
</script>
@endsection