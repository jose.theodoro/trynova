@extends('layout.base')

@section('title', 'Tipo de Unidade')
@section('page-title', 'Tipo de Unidade')
@section('page-title-description', 'Tipo de Unidade Cadastradas')


@section('content')
<div class="dropdown d-inline-block">
    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">
        <i class="fa fa-bars"></i> Ações
    </button>
    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-hover-link dropdown-menu">
        <button type="button" tabindex="0" class="dropdown-item">
         <a href="{{route('property_type.create')}}">
         <i class="dropdown-icon fa fa-plus"> </i> Novo Tipo de unidade
         </a>
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <table style="width: 100%;" id="data-table" class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 40px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Tipo de Unidade</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 5px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($propertyTypes as $propertyType)
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">{{$propertyType->name}}</td>
                            <td class="text-center">
                                <a href="{{route('property_type.edit', $propertyType->id)}}">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <a href="javascript:;" onclick="confirmDelete(this)" data-href="{{route('property_type.delete', $propertyType->id)}}">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>





@endsection
