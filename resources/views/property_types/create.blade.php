@extends('layout.base')

@section('title', 'Tipos de Unidade')
@section('page-title', 'Tipos de Unidade')
@section('page-title-description', 'cire um novo tipo de unidade.')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('property_type.store')}}">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label class="">Tipos de Unidade</label>
                                <input name="name" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('property_type.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection