@extends('layout.base')

@section('title', 'Atividades')
@section('page-title', 'Atividades')
@section('page-title-description', 'cire uma nova atividade.')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('task.update')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{$task->id}}">
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label class="">Atividade</label>
                                <input name="name" type="text" class="form-control" value="{{$task->name}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label class="">Descrição da Atividade</label>
                                <textarea class="form-control" name="description" id="" cols="20" rows="5">{{$task->description}}</textarea>
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('task.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection