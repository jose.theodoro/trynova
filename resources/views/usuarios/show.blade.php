@extends('layout.base')

@section('title', 'Usuários')
@section('page-title', 'Usuários')
@section('page-title-description', 'mais detalhes sobre o usuário Fulano da Silva')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
                <div class="card-body">

                <form class="">
                                                                <div class="form-row">
                                                                    <div class="col-md-4">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleEmail11" class="">Nome</label>
                                                                            <input name="carteira" type="text" class="form-control" value="Maria dos Santos Alves">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="position-relative form-group">
                                                                            <label for="examplePassword11" class="">Perfil</label>
                                                                            <select class="form-control" name="gerente" id="">
                                                                                <option value="1">Gerente de Contas</option>
                                                                                <option value="1">Super Usuário</option>
                                                                                <option value="1">Assistente</option>
                                                                                <option value="1">Consulta</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="position-relative form-group">
                                                                        <label for="examplePassword11" class="">Departamento</label>
                                                                            <select class="form-control" name="gerente" id="">
                                                                                <option value="1">Renovação</option>
                                                                                <option value="1">Fiscal</option>
                                                                                <option value="1">Técnica</option>
                                                                                <option value="1">Externo</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <div class="col-md-6">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleCity" class="">Email</label>
                                                                            <input name="city" type="text" class="form-control" value="Email">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="position-relative form-group">
                                                                        <label for="examplePassword11" class="">Empresa</label>
                                                                            <select class="form-control" name="gerente" id="">
                                                                                <option value="1">Trynova</option>
                                                                                <option value="1">Santander</option>
                                                                                <option value="1">Youcom</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleZip" class="">Data de Criação</label>
                                                                            <input name="created_at" disabled="true" type="text" class="form-control" value="30/08/2019">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <div class="col-md-6">
                                                                        <div class="position-relative form-group">
                                                                        <label for="exampleCity" class="">Carteira</label>
                                                                            <select multiple="multiple" class="multiselect-dropdown form-control">
                                                                                <option selected value="AK" data-select2-id="73">Santander</option>
                                                                                <option value="HI" data-select2-id="74">Youcom</option>
                                                                                <option value="CA" data-select2-id="76">Renner</option>
                                                                                <option value="CA" data-select2-id="79">Carteira 01</option>
                                                                                <option value="CA" data-select2-id="70">Carteira 02</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="position-relative form-group">
                                                                        <label for="exampleCity" class="">Região</label>
                                                                            <select multiple="multiple" class="multiselect-dropdown form-control">
                                                                                <option selected value="AK" data-select2-id="8">São Paulo Capital</option>
                                                                                <option value="HI" data-select2-id="74">Metropolitana São Paulo</option>
                                                                                <option value="CA" data-select2-id="76">Metropolitana Rio de Janeiro</option>
                                                                                <option value="CA" data-select2-id="79">Estado de São Paulo</option>
                                                                                <option value="CA" data-select2-id="70">Região leste do Sergipe</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class="col-md-2"> -->
                                                                <a class="mt-2 btn btn-secondary" href="{{route('carteiras.index')}}">Voltar</a>
                                                                <a class="mt-2 btn btn-alternate" href="#">Salvar</a>
                                                                <!-- </div> -->
                                                            </form>



                                                            




                </div>
        </div>
    </div>
</div>
@endsection