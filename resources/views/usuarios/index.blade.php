@extends('layout.base')

@section('title', 'Usuários')
@section('page-title', 'Usuários')
@section('page-title-description', 'Usuários Cadastrados')


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <table style="width: 100%;" id="data-table" class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 40px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Nome</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 20px;" aria-label="Age: activate to sort column ascending">Email</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 20px;" aria-label="Age: activate to sort column ascending">Perfil</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 20px;" aria-label="Age: activate to sort column ascending">Empresa</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 5px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Maria dos Santos Alves</td>
                            <td>maria@trynova.adm.br</td>
                            <td>Gerente de Contas</td>
                            <td>Trynova</td>
                            <td class="text-center">
                                <a href="{{route('usuarios.show', 2)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">José da Silva</td>
                            <td>jose@trynova.adm.br</td>
                            <td>Super Usuário</td>
                            <td>Trynova</td>
                            <td class="text-center">
                                <a href="{{route('usuarios.show', 2)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Fulano da Silva</td>
                            <td>fulano@santander.com.br</td>
                            <td>Consulta</td>
                            <td>Santander</td>
                            <td class="text-center">
                                <a href="{{route('usuarios.show', 2)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection