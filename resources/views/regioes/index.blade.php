@extends('layout.base')

@section('title', 'Regiões')
@section('page-title', 'Regiões')
@section('page-title-description', 'Regiões Cadastradas')


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <table style="width: 100%;" id="data-table" class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 30px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Região</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 20px;" aria-label="Age: activate to sort column ascending">Estado</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 20px;">Cidades</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 15px;" aria-label="Start date: activate to sort column ascending">Data de Criação</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 5px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Metropolitana de São Paulo</td>
                            <td>São Paulo</td>
                            <td>Guarulhos, Mogi das Cruzes, Diadema, Santa Isabel ...</td>
                            <td>30/08/19</td>
                            <td class="text-center">
                                <a href="{{route('regioes.show', 2)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Região qualquer</td>
                            <td>Rio de Janeiro</td>
                            <td>Niteroi, São Gonçalo, Duque de Caxias, Petrópolis ...</td>
                            <td>30/09/19</td>
                            <td class="text-center">
                                <a href="#">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection