@extends('layout.base')

@section('title', 'Regiões')
@section('page-title', 'Regiões')
@section('page-title-description', 'mais detalhes sobre a região Metropolitana de São Paulo')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
                <div class="card-body">

                <form class="">
                                                                <div class="form-row">
                                                                    <div class="col-md-6">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleEmail11" class="">Região</label>
                                                                            <input name="carteira" type="text" class="form-control" value="Metropolitana de São Paulo">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="position-relative form-group">
                                                                            <label for="examplePassword11" class="">Estado</label>
                                                                            <select class="form-control" name="gerente" id="">
                                                                                <option value="1">São Paulo</option>
                                                                                <option value="1">Rio de Janeiro</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <div class="col-md-10">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleCity" class="">Cidades</label>
                                                                            <select multiple="multiple" class="multiselect-dropdown form-control">
                                                                                <option value="AK" data-select2-id="73">São Paulo</option>
                                                                                <option value="HI" data-select2-id="74">Mogi das Cruzes</option>
                                                                                <option value="CA" data-select2-id="76">Santa Isabel</option>
                                                                                <option value="NV" data-select2-id="77">Guarulhos</option>
                                                                                <option value="OR" data-select2-id="78">Diedema</option>
                                                                                <option value="WA" data-select2-id="79">Campinas</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="position-relative form-group">
                                                                            <label for="exampleZip" class="">Data de Criação</label>
                                                                            <input name="created_at" disabled="true" type="text" class="form-control" value="30/08/2019">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class="col-md-2"> -->
                                                                <a class="mt-2 btn btn-secondary" href="{{route('regioes.index')}}">Voltar</a>
                                                                <a class="mt-2 btn btn-alternate" href="#">Salvar</a>
                                                                <!-- </div> -->
                                                            </form>




                </div>
        </div>
    </div>
</div>
@endsection