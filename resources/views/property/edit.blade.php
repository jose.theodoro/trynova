@extends('layout.base')

@section('title', 'Unidades')
@section('page-title', 'Unidades')
@section('page-title-description', "mais informações da unidade $property->name")

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('property.update')}}">
                    @csrf
                    <input name="id" type="hidden" value="{{$property->id}}">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label class="">Nome da Unidade</label>
                                <input name="name" type="text" class="form-control" value="{{$property->name}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label class="">Tipo da Unidade</label>
                                <select name="property_type_id" class="form-control">
                                    @foreach($propertyTypes as $type)
                                        <option {{$type->id == $property->property_type_id ? 'selected' : ''}} value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                    <label class="">Cliente</label>
                                    <select name="customer_id" class="form-control">
                                        @foreach($customers as $customer)
                                            <option {{$customer->id == $property->customer_id ? 'selected' : ''}} value="{{$customer->id}}">{{$customer->name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                    <label class="">Região</label>
                                    <select name="region_id" class="form-control">
                                        @foreach($regions as $region)
                                            <option {{$region->id == $property->region_id ? 'selected' : ''}} value="{{$region->id}}">{{$region->name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="position-relative form-group">
                                    <label class="">CNPJ</label>
                                    <input name="cnpj" class="form-control" value="{{$property->cnpj}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="position-relative form-group">
                                    <label class="">CEP</label>
                                    <input name="cep" class="form-control" value="{{$property->cep}}">
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('property.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection