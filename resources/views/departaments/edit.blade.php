@extends('layout.base')

@section('title', 'Area')
@section('page-title', 'Area')
@section('page-title-description', 'cire uma nova área.')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('departaments.update')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{$departament->id}}">
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label for="exampleEmail11" class="">Area</label>
                                <input name="name" type="text" class="form-control" value="{{$departament->name}}">
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('departaments.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection