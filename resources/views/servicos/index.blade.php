@extends('layout.base')

@section('title', 'Serviços')
@section('page-title', 'Serviços')
@section('page-title-description', 'Serviços Cadastrados')


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <table style="width: 100%;" id="data-table" class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 40px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Serviço</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 20px;" aria-label="Age: activate to sort column ascending">Tipo do Serviço ?</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 5px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Licença Ambiental</td>
                            <td>Regularização Documental</td>
                            <td class="text-center">
                                <a href="{{route('servicos.show', 2)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Licença Aalvará</td>
                            <td>Regularização Documental</td>
                            <td class="text-center">
                            <a href="{{route('servicos.show', 2)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Regularizar Razão Social</td>
                            <td>Regularização Cadastral</td>
                            <td class="text-center">
                                <a href="#">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection