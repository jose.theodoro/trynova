@extends('layout.base')

@section('title', 'Usuários')
@section('page-title', 'Usuários')
@section('page-title-description', 'cire um novo cliente.')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('user.store')}}">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-8">
                            <div class="position-relative form-group">
                                <label for="name" class="">Nome</label>
                                <input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label class="">Departamento</label>
                                <select name="departament_id" class="form-control">
                                    @foreach($departaments as $departament)
                                        <option {{{$departament->id == old("departament_id") ? "selected" : "" }}} value="{{$departament->id}}">{{$departament->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-8">
                            <div class="position-relative form-group">
                                <label for="email" class="">Email</label>
                                <input id="email" name="email" type="email" class="form-control" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="profile" class="">Perfil</label>
                                <select name="profile_id" class="form-control">
                                    @foreach($profiles as $profile)
                                        <option {{{$profile->id == old("profile_id") ? "selected" : "" }}} value="{{$profile->id}}">{{$profile->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label for="password" class="">Senha</label>
                                <input id="password" name="password" type="password" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label for="password_confirm" class="">Confirmação de Senha</label>
                                <input id="password_confirm" name="password_confirmation" type="password" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label for="customer" class="">Clientes</label>
                                <div class="position-relative form-group">
                                    @foreach($customers as $customer)
                                        <div class="custom-checkbox custom-control custom-control-inline">
                                            @if(old('customers'))
                                                @foreach(old('customers') as $c)
                                                    @if($c == $customer->id)
                                                        <input name="customers[{{$customer->id}}]" value="{{$customer->id}}" checked type="checkbox" id="customer_{{$customer->id}}" class="custom-control-input">
                                                        @continue
                                                    @endif
                                                @endforeach
                                            @endif
                                            <input name="customers[{{$customer->id}}]" value="{{$customer->id}}" type="checkbox" id="customer_{{$customer->id}}" class="custom-control-input">
                                            <label class="custom-control-label" for="customer_{{$customer->id}}">
                                                {{$customer->name}}
                                            </label>
                                        </div>
                                    @endforeach 
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('user.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection