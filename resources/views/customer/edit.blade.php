@extends('layout.base')

@section('title', 'Cliente')
@section('page-title', 'Cliente')
@section('page-title-description', 'cire um novo cliente.')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form method="POST" action="{{route('customer.update')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{$customer->id}}">
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label class="">Cliente</label>
                                <input name="name" type="text" class="form-control" value="{{$customer->name}}">
                            </div>
                        </div>
                    </div>
                    <a class="mt-2 btn btn-secondary" href="{{route('customer.index')}}">Voltar</a>
                    <button type="submit" class="mt-2 btn btn-alternate" href="#">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection