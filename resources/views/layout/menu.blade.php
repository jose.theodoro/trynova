<div class="app-sidebar-wrapper">
                <div class="app-sidebar sidebar-shadow">
                    <div class="app-header__logo">
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="KeroUI Admin Template" class="logo-src"></a>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                        </button>
                    </div>
                    <div class="scrollbar-sidebar scrollbar-container">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">

                                <li class="app-sidebar__heading">Menu</li>
                                <li><a href="{{route('dashboard.index')}}"><i class="metismenu-icon pe-7s-graph2"></i>Resumo</a></li>

                                <li class="app-sidebar__heading">Gerencial</li>
                               
                                <li><a href="{{route('departaments.index')}}"><i class="metismenu-icon pe-7s-map-marker"></i>Departamentos</a></li>
                                <li><a href="{{route('region.index')}}"><i class="metismenu-icon pe-7s-map-marker"></i>Regiões</a></li>
                                <li><a href="{{route('customer.index')}}"><i class="metismenu-icon pe-7s-wallet"></i>Clientes</a></li>
                                <li><a href="{{route('property.index')}}"><i class="metismenu-icon pe-7s-home"></i>Unidades</a></li>
                                <li><a href="{{route('service.index')}}"><i class="metismenu-icon pe-7s-news-paper"></i>Serviços</a></li>
                                <li><a href="{{route('task.index')}}"><i class="metismenu-icon pe-7s-plugin"></i>Atividades</a></li>
                                <li><a href="{{route('property_type.index')}}"><i class="metismenu-icon pe-7s-home"></i>Tipos de Unidades</a></li>
                                <li><a href="#"><i class="metismenu-icon pe-7s-plugin"></i>Feriados</a></li>
                                

                                <!-- <li class="app-sidebar__heading">Operacional</li>
                                <li><a href="{{route('os.index')}}"><i class="metismenu-icon pe-7s-file"></i>Abrir Ordem de Serviço</a></li> -->
                                
                                <li class="app-sidebar__heading">Acesso e Segurança</li>
                                <li><a href="{{route('user.index')}}"><i class="metismenu-icon pe-7s-users"></i>Usuários</a></li>
                                <li><a href="{{route('profiles.index')}}"><i class="metismenu-icon pe-7s-unlock"></i>Perfis e Permissões</a></li>

                                <li class="app-sidebar__heading">Configurações</li>
                                <li><a href="#"><i class="metismenu-icon pe-7s-bell"></i>Alarmes</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>