@extends('layout.base')

@section('title', 'Unidades')
@section('page-title', 'Unidades')
@section('page-title-description', 'mais detalhes sobre a unidade Santander X123')

@section('content')

<div class="mb-3 card">
<div class="card-header">
<ul class="nav nav-justified">
<li class="nav-item">
<a data-toggle="tab" href="#tab-eg7-0" class="nav-link show active">
Geral
</a>
</li>
<li class="nav-item">
<a data-toggle="tab" href="#tab-eg7-1" class="nav-link show">
Licenças
</a>
</li>
</ul>
</div>
<div class="card-body">
<div class="tab-content">
<div class="tab-pane show active" id="tab-eg7-0" role="tabpanel">
<div class="row">
<div class="col-md-12">
<div class="card-body">
<form class="">
<div class="form-row">
<div class="col-md-4">
<div class="position-relative form-group">
<label for="exampleEmail11" class="">Unidade</label>
<input name="carteira" type="text" class="form-control" value="Satander Brasil">
</div>
</div>
<div class="col-md-4">
<div class="position-relative form-group">
<label for="examplePassword11" class="">Carteira</label>
<select class="form-control" name="gerente" id="">
<option value="1">Santander</option>
<option value="1">Youcom</option>
</select>
</div>
</div>
<div class="col-md-4">
<div class="position-relative form-group">
<label for="examplePassword11" class="">Categoria / Tipo</label>
<select class="form-control" name="gerente" id="">
<option value="1">Predial</option>
<option value="1">Sobrado</option>
<option value="1">Restaurante</option>
</select>
</div>
</div>
</div>
<div class="form-row">
<div class="col-md-4">
<div class="position-relative form-group">
<label for="exampleState" class="">Região</label>
<select class="form-control" name="gerente" id="">
<option value="1">Metropolitana São Paulo</option>
<option value="1">Regiao qualquer</option>
</select>
</div>
</div>
<div class="col-md-2">
<div class="position-relative form-group">
<label for="exampleCity" class="">CNPJ</label>
<input name="city" type="text" class="form-control" value="02324903000153">
</div>
</div>
<div class="col-md-4">
<div class="position-relative form-group">
<label for="exampleCity" class="">Endereço</label>
<input name="city" type="text" class="form-control" value="Av Paulista 123">
</div>
</div>
<div class="col-md-1">
<div class="position-relative form-group">
<label for="exampleState" class="">UNIORG</label>
<input name="state" type="text" class="form-control" value="143XC123">
</div>
</div>
<div class="col-md-1">
<div class="position-relative form-group">
<label for="exampleZip" class="">Data de Criação</label>
<input name="created_at" disabled="true" type="text" class="form-control" value="30/08/2019">
</div>
</div>
</div>
<!-- <div class="col-md-2"> -->
<a class="mt-2 btn btn-secondary" href="{{route('carteiras.index')}}">Voltar</a>
<a class="mt-2 btn btn-alternate" href="#">Salvar</a>
<!-- </div> -->
</form>
</div>
</div>
</div>
</div>
<div class="tab-pane show" id="tab-eg7-1" role="tabpanel">
<div class="row">
<div class="col-md-12">
<!-- <div class="main-card mb-3 card"> -->
<!-- <div class="card-header">Licenças</div> -->
<div class="card-body">
<table class="table table-borderless">
<thead>
<tr>
<th> </th>
<th>Emitida em</th>
<th>Expira em</th>
<th>Situação</th>
<th>O.S</th>
</tr>
</thead>
<tbody>
<tr>
<td>Alvará</td>
<td>30/08/2019</td>
<td>30/08/2019</td>
<td><div class="mb-2 mr-2 badge badge-success">REGULAR</div></td>
<td><a class="mb-2 mr-2 btn btn-light" href="#">OS-12345</a></td>
</tr>
<tr>
<td>AVCB</td>
<td>30/06/2018</td>
<td>30/06/2019</td>
<td><div class="mb-2 mr-2 badge badge-danger">VENCIDA</div></td>
<td></td>
</tr>
<tr>
<td>Ambiental</td>
<td>30/08/2019</td>
<td>30/08/2019</td>
<td><div class="mb-2 mr-2 badge badge-success">REGULAR</div></td>
<td><a class="mb-2 mr-2 btn btn-light" href="#">OS-12345</a></td>
</tr>
</tbody>
</table>
</div>
<!-- </div> -->
</div>
</div>
</div>
</div>
</div>
</div>
@endsection