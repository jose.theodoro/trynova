@extends('layout.base')

@section('title', 'Unidades')
@section('page-title', 'Unidades')
@section('page-title-description', 'Unidades Cadastradas')


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <table style="width: 100%;" id="data-table" class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 40px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Unidade</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 20px;" aria-label="Age: activate to sort column ascending">Carteira</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 10px;">Uniorg</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 10px;">Região</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 10px;" aria-label="Start date: activate to sort column ascending">CNPJ</th>
                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 5px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Santander X123</td>
                            <td>Santander</td>
                            <td>143XC123</td>
                            <td>Metropolitana São Paulo</td>
                            <td>02324903000153</td>
                            <td class="text-center">
                                <a href="{{route('unidades.show', 2)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td tabindex="0" class="sorting_1">Youcom Z123</td>
                            <td>Youcom</td>
                            <td>135HY321</td>
                            <td>Qualquer Região</td>
                            <td>02324903000153</td>
                            <td class="text-center">
                                <a href="#">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection