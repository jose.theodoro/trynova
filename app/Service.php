<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Region;
use App\Customer;
use App\Task;

class Service extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'region_id', 'customer_id', 'description', 'price', 'custom_id'];

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }

}
