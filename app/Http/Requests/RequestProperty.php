<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestProperty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'property_type_id' => 'required',
            'customer_id' => 'required',
            'region_id' => 'required',
            'cep' => 'required',
            'cnpj' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nome da unidade não pode ser vazio.',
            'property_type_id.required' => 'Tipo da unidade não pode ser vazio.',
            'customer_id.required' => 'Cliente não pode ser vazio.',
            'region_id.required' => 'Região não pode ser vazio.',
            'cep.required' => 'CEP não pode ser vazio.',
            'cnpj.required' => 'CNPJ não pode ser vazio.',
        ];
    }
}
