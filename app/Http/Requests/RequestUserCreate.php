<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestUserCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' =>'required|email',
            'password' =>'required|confirmed|min:6',
            'departament_id' => 'required',
            'profile_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nome não pode ser vazio.',
            'email.required' =>'Email não pode ser vazio.',
            'email.email' =>'Email inválido.',
            'password.required' =>'Senha não pode ser vazio.',
            'password.confirmed' =>'As senhas digitas são diferentes.',
            'password.min' =>'A senha deve ter no mínimo 6 caracterers.',
            'departament_id.required' => 'Departamento não pode ser vazio.',
            'profile_id.required' => 'Perfil não pode ser vazio.',
        ];
    }
}
