<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestUserUpdate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' =>'required|email',
            'departament_id' => 'required',
            'profile_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nome não pode ser vazio.',
            'email.required' =>'Email não pode ser vazio.',
            'email.email' =>'Email inválido.',
            'departament_id.required' => 'Departamento não pode ser vazio.',
            'profile_id.required' => 'Perfil não pode ser vazio.',
        ];
    }
}
