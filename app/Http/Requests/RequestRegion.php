<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestRegion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'state_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nome da região não pode ser vazio.',
            'state_id.required' => 'Estado não pode ser vazio.',
        ];
    }
}
