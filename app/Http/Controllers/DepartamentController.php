<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestDepartaments;
use App\Departament;

class DepartamentController extends Controller
{
    public function index()
    {
        $departaments = Departament::all();
        return view('departaments.index')->with(compact('departaments'));
    }

    public function create()
    {
        return view('departaments.create');
    }

    public function store(RequestDepartaments $request)
    {
        if(Departament::create($request->only('name'))) {
            return redirect()->back()->withSuccess('Departamento cadastrado.');
        }
        return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao criar o departamento', 'error'=> true]);

    }

    public function edit($id)
    {
        $departament = Departament::find($id);
        return view('departaments.edit')->with(compact('departament'));
    }

    public function update(RequestDepartaments $request)
    {
        try {
            $departament = Departament::findOrFail($request->get('id'));
            $departament->name = $request->get('name');
            $departament->save();
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao atualizar o departamento']);
        }
        return redirect()->back()->withSuccess('Departamento atualizado.');
    }

    public function delete($id)
    {
        try {
            $departament = Departament::findOrFail($id);
            $departament->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao remover o departamento']);
        }
        return redirect()->back()->withSuccess('Departamento removido.');
    }

}
