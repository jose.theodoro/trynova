<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestUserCreate;
use App\Http\Requests\RequestUserUpdate;

use App\User;
use App\Departament;
use App\Profile;
use App\Customer;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('user.index')->with(compact('users'));
    }

    public function create()
    {
        $departaments = Departament::orderBy('name')->get();
        $profiles = Profile::orderBy('name')->get();
        $customers = Customer::orderby('name')->get();

        return view('user.create')->with(compact('departaments', 'profiles', 'customers'));
    }

    public function store(RequestUserCreate $request)
    {
        try {
            $request['password'] = bcrypt($request->get('password'));
            $user = User::create($request->only('name', 'email', 'password', 'departament_id', 'profile_id'));
            if($user) {
                $user = User::find($user->id);
                $user->customers()->attach($request->get('customers'));
                return redirect()->back()->withSuccess('Usuário cadastrado.');
            }
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao criar o cliente', 'error'=> true]);
        }

    }

    public function edit($id)
    {
        $departaments = Departament::orderBy('name')->get();
        $profiles = Profile::orderBy('name')->get();
        $customers = Customer::orderby('name')->get();

        $user = User::find($id);
        return view('user.edit')->with(compact('user', 'departaments', 'profiles', 'customers'));
    }

    public function update(RequestUserUpdate $request)
    {
        try {
            $user = User::findOrFail($request->get('id'));
            $user->fill($request->only('name, email, departament_id, profile_id'))->save();
            $user->customers()->sync($request->get('customers'));
        } catch (\Exception $th) {
            // return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao atualizar o cliente']);
            return redirect()->back()->withErrors([$th->getMessage()]);
        }
        return redirect()->back()->withSuccess('Usuário atualizado.');
    }

    public function delete($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao remover o cliente']);
        }
        return redirect()->back()->withSuccess('Usuário removido.');
    }

}
