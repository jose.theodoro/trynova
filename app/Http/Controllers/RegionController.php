<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestRegion;
use App\Region;
use App\State;

class RegionController extends Controller
{
    public function index()
    {
        $regions = Region::all();
        return view('region.index')->with(compact('regions'));
    }

    public function create()
    {
        $states = State::orderBy('name')->get();
        return view('region.create')->with(compact('states'));
    }

    public function store(RequestRegion $request)
    {
        try {
            $region = Region::create($request->all());
            if($region) {
                $region = Region::find($region->id);
                $region->cities()->attach($request->get('cities'));
            }
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao criar a região', 'error'=> true]);
        }
        return redirect()->back()->withSuccess('Região cadastrada.');

    }

    public function edit($id)
    {
        $region = Region::find($id);
        $states = State::orderBy('name')->get();
        return view('region.edit')->with(compact('region', 'states'));
    }

    public function update(RequestRegion $request)
    {
        try {
            $region = Region::findOrFail($request->get('id'));
            $region->fill($request->all());
            $region->save();
            $region->cities()->sync($request->get('cities'));
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao atualizar a região']);
        }
        return redirect()->back()->withSuccess('Região atualizada.');
    }

    public function delete($id)
    {
        try {
            $region = Region::findOrFail($id);
            $region->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao remover a região']);
        }
        return redirect()->back()->withSuccess('Região removida.');
    }

    public function findCitiesByState($id)
    {
        $state = State::find($id);
        return response()->json(['data' => $state->cities]);
    }
}
