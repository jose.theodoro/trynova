<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestTask;
use App\Task;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('task.index')->with(compact('tasks'));
    }

    public function create()
    {
        return view('task.create');
    }

    public function store(RequestTask $request)
    {
        if(Task::create($request->all())) {
            return redirect()->back()->withSuccess('Atividade cadastrada.');
        }
        return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao criar a atividade', 'error'=> true]);

    }

    public function edit($id)
    {
        $task = Task::find($id);
        return view('task.edit')->with(compact('task'));
    }

    public function update(RequestTask $request)
    {
        try {
            $task = Task::findOrFail($request->get('id'));
            $task->fill($request->all());
            $task->save();
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao atualizar a atividade']);
        }
        return redirect()->back()->withSuccess('Atividade atualizada.');
    }

    public function delete($id)
    {
        try {
            $task = Task::findOrFail($id);
            $task->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao remover a atividade']);
        }
        return redirect()->back()->withSuccess('Atividade removida.');
    }

}
