<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestProfiles;
use App\Profile;

class ProfileController extends Controller
{
    public function index()
    {
        $profiles = Profile::all();
        return view('profiles.index')->with(compact('profiles'));
    }

    public function create()
    {
        return view('profiles.create');
    }

    public function store(RequestProfiles $request)
    {
        if(Profile::create($request->only('name'))) {
            return redirect()->back()->withSuccess('Perfil cadastrado.');
        }
        return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao criar o perfil', 'error'=> true]);

    }

    public function edit($id)
    {
        $profile = Profile::find($id);
        return view('profiles.edit')->with(compact('profile'));
    }

    public function update(RequestProfiles $request)
    {
        try {
            $profile = Profile::findOrFail($request->get('id'));
            $profile->name = $request->get('name');
            $profile->save();
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao atualizar o perfil']);
        }
        return redirect()->back()->withSuccess('Perfil atualizado.');
    }

    public function delete($id)
    {
        try {
            $profile = Profile::findOrFail($id);
            $profile->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao remover o perfil']);
        }
        return redirect()->back()->withSuccess('Perfil removido.');
    }

}
