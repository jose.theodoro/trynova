<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestService;
use App\Service;
use App\Region;
use App\Customer;
use App\Task;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::all();
        return view('service.index')->with(compact('services'));
    }

    public function create()
    {
        $tasks = Task::orderBy('name')->get();
        $customers = Customer::orderBy('name')->get();
        $regions = Region::orderBy('name')->get();
        return view('service.create')->with(compact('customers', 'regions', 'tasks'));
    }

    public function store(RequestService $request)
    {
        try {
            $service = Service::create($request->all());
            if($service) {
                $service->tasks()->sync($request->get('tasks'));
            }
        } catch (\Exception $th) {
            dd($th->getMessage());
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao criar o serviço', 'error'=> true]);
        }
        
        return redirect()->back()->withSuccess('Serviço cadastrada.');

    }

    public function edit($id)
    {
        $tasks = Task::orderBy('name')->get();
        $customers = Customer::orderBy('name')->get();
        $regions = Region::orderBy('name')->get();
        $service = Service::find($id);
        return view('service.edit')->with(compact('service', 'customers', 'regions', 'tasks'));
    }

    public function update(RequestService $request)
    {
        try {
            $service = Service::findOrFail($request->get('id'));
            $service->fill($request->all());
            $service->save();
            $service->tasks()->sync($request->get('tasks'));
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao atualizar o serviço']);
        }
        return redirect()->back()->withSuccess('Serviço atualizado.');
    }

    public function delete($id)
    {
        try {
            $service = Service::findOrFail($id);
            $service->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao remover o serviço']);
        }
        return redirect()->back()->withSuccess('Serviço removido.');
    }
}
