<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PropertyType;
Use App\Http\Requests\RequestPropertyType;

class PropertyTypeController extends Controller
{
    public function index()
    {
        $propertyTypes = PropertyType::all();
        return view('property_types.index')->with(compact('propertyTypes'));
    }

    public function create()
    {
        return view('property_types.create');
    }

    public function store(RequestPropertyType $request)
    {
        if(PropertyType::create($request->all())) {
            return redirect()->back()->withSuccess('Tipo de unidade cadastrado.');
        }
        return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao criar o tipo de unidade', 'error'=> true]);

    }

    public function edit($id)
    {
        $propertyType = PropertyType::find($id);
        return view('property_types.edit')->with(compact('propertyType'));
    }

    public function update(RequestPropertyType $request)
    {
        try {
            $propertyType = PropertyType::findOrFail($request->get('id'));
            $propertyType->fill($request->all());
            $propertyType->save();
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao atualizar o tipo de unidade']);
        }
        return redirect()->back()->withSuccess('Tipo de unidade atualizado.');
    }

    public function delete($id)
    {
        try {
            $propertyType = PropertyType::findOrFail($id);
            $propertyType->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao remover o tipo de unidade']);
        }
        return redirect()->back()->withSuccess('Tipo de unidade removido.');
    }
}
