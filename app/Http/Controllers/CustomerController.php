<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestCustomer;
use App\Customer;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::all();
        return view('customer.index')->with(compact('customers'));
    }

    public function create()
    {
        return view('customer.create');
    }

    public function store(RequestCustomer $request)
    {
        if(Customer::create($request->only('name'))) {
            return redirect()->back()->withSuccess('Cliente cadastrado.');
        }
        return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao criar o cliente', 'error'=> true]);

    }

    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customer.edit')->with(compact('customer'));
    }

    public function update(RequestCustomer $request)
    {
        try {
            $customer = Customer::findOrFail($request->get('id'));
            $customer->name = $request->get('name');
            $customer->save();
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao atualizar o cliente']);
        }
        return redirect()->back()->withSuccess('Cliente atualizado.');
    }

    public function delete($id)
    {
        try {
            $customer = Customer::findOrFail($id);
            $customer->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao remover o cliente']);
        }
        return redirect()->back()->withSuccess('Cliente removido.');
    }

}
