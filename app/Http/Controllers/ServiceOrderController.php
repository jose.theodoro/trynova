<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class ServiceOrderController extends Controller
{
    public function create()
    {
        $customers = Customer::orderBy('name')->get();
        return view('os.create', compact('customers'));
    }
}
