<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestProperty;
use App\Property;
use App\Customer;
use App\Region;
use App\PropertyType;

class PropertyController extends Controller
{
    public function index()
    {
        $properties = Property::all();
        return view('property.index')->with(compact('properties'));
    }

    public function create()
    {
        $propertyTypes = PropertyType::orderBy('name')->get();
        $customers = Customer::orderBy('name')->get();
        $regions = Region::orderBy('name')->get();
        return view('property.create')->with(compact('propertyTypes', 'customers', 'regions'));
    }

    public function store(RequestProperty $request)
    {
        if(Property::create($request->all())) {
            return redirect()->back()->withSuccess('Unidade cadastrada.');
        }
        return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao criar a unidade', 'error'=> true]);

    }

    public function edit($id)
    {
        $propertyTypes = PropertyType::orderBy('name')->get();
        $customers = Customer::orderBy('name')->get();
        $regions = Region::orderBy('name')->get();
        $property = Property::find($id);
        return view('property.edit')->with(compact('property', 'propertyTypes', 'customers', 'regions'));
    }

    public function update(RequestProperty $request)
    {
        try {
            $property = Property::findOrFail($request->get('id'));
            $property->fill($request->all());
            $property->save();
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao atualizar a unidade']);
        }
        return redirect()->back()->withSuccess('Unidade atualizada.');
    }

    public function delete($id)
    {
        try {
            $property = Property::findOrFail($id);
            $property->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Ocorreu um problema ao remover a unidade']);
        }
        return redirect()->back()->withSuccess('Unidade removida.');
    }
}
