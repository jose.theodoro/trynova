<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Customer;
use App\Property;
use App\Service;

class ServiceOrder extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'customer_id', 'property_id', 'service_id', 'service_order_parent_id', 'nro', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function childs()
    {
        return $this->hasMany(ServiceOrder::class, 'service_order_parent_id');
    }
}
