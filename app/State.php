<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Country;
use App\City;

class State extends Model
{
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
