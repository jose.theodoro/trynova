<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\State;
use App\Region;

class City extends Model
{
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function regions()
    {
        return $this->belongsToMany(Region::class);
    }
}
