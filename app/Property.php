<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Customer;
use App\Region;

class Property extends Model
{
    use SoftDeletes;

    protected $fillable = ['property_type_id','customer_id','region_id','name', 'cep', 'cnpj'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
