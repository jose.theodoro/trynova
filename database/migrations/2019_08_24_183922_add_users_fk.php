<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('departament_id');
            $table->unsignedBigInteger('profile_id');

            $table->foreign('departament_id')->references('id')->on('departaments');
            $table->foreign('profile_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_departament_id_foreign');
            $table->dropForeign('users_profile_id_foreign');
            $table->dropColumn('departament_id');
            $table->dropColumn('profile_id');
        });
    }
}
