<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('property_type_id');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('region_id');
            $table->string('name');
            $table->integer('cnpj');
            $table->integer('cep');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('property_type_id')->references('id')->on('property_types');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('region_id')->references('id')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
