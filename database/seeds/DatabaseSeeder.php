<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        Eloquent::unguard();

        // $path = 'app/Trynova/Data/Mysql/countries.sql';
        // DB::unprepared(file_get_contents($path));
        // $this->command->info('Country table seeded!');

        // $path = 'app/Trynova/Data/Mysql/states.sql';
        // DB::unprepared(file_get_contents($path));
        // $this->command->info('States table seeded!');

        $path = 'app/Trynova/Data/Mysql/cities.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Cities table seeded!');
    }
}
