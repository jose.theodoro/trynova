function confirmDelete(el)
{
    const uriDelete = $(el).attr('data-href');
    
    const q = confirm("Deseja deletar esse registro?");

    if(q) window.location = uriDelete

    return false;

}