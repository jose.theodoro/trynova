<?php

// DASHBOARD
Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

// DEPARTAMENTOS
Route::get('/departaments','DepartamentController@index')->name('departaments.index');
Route::get('/departaments/edit/{id}','DepartamentController@edit')->name('departaments.edit');
Route::get('/departaments/delete/{id}','DepartamentController@delete')->name('departaments.delete');
Route::get('/departaments/create','DepartamentController@create')->name('departaments.create');
Route::post('/departaments/store','DepartamentController@store')->name('departaments.store');
Route::post('/departaments/update','DepartamentController@update')->name('departaments.update');

// PERFIL
Route::get('/profiles','ProfileController@index')->name('profiles.index');
Route::get('/profiles/edit/{id}','ProfileController@edit')->name('profiles.edit');
Route::get('/profiles/delete/{id}','ProfileController@delete')->name('profiles.delete');
Route::get('/profiles/create','ProfileController@create')->name('profiles.create');
Route::post('/profiles/store','ProfileController@store')->name('profiles.store');
Route::post('/profiles/update','ProfileController@update')->name('profiles.update');

// CLIENTES
Route::get('/customer','CustomerController@index')->name('customer.index');
Route::get('/customer/edit/{id}','CustomerController@edit')->name('customer.edit');
Route::get('/customer/delete/{id}','CustomerController@delete')->name('customer.delete');
Route::get('/customer/create','CustomerController@create')->name('customer.create');
Route::post('/customer/store','CustomerController@store')->name('customer.store');
Route::post('/customer/update','CustomerController@update')->name('customer.update');

// USUÁRIOS
Route::get('/user','UserController@index')->name('user.index');
Route::get('/user/edit/{id}','UserController@edit')->name('user.edit');
Route::get('/user/delete/{id}','UserController@delete')->name('user.delete');
Route::get('/user/create','UserController@create')->name('user.create');
Route::post('/user/store','UserController@store')->name('user.store');
Route::post('/user/update','UserController@update')->name('user.update');

// ATIVIDADES
Route::get('/task','TaskController@index')->name('task.index');
Route::get('/task/edit/{id}','TaskController@edit')->name('task.edit');
Route::get('/task/delete/{id}','TaskController@delete')->name('task.delete');
Route::get('/task/create','TaskController@create')->name('task.create');
Route::post('/task/store','TaskController@store')->name('task.store');
Route::post('/task/update','TaskController@update')->name('task.update');

// REGIOES
Route::get('/region','RegionController@index')->name('region.index');
Route::get('/region/edit/{id}','RegionController@edit')->name('region.edit');
Route::get('/region/delete/{id}','RegionController@delete')->name('region.delete');
Route::get('/region/create','RegionController@create')->name('region.create');
Route::post('/region/store','RegionController@store')->name('region.store');
Route::post('/region/update','RegionController@update')->name('region.update');
Route::get('/region/state/{id}/cities','RegionController@findCitiesByState')->name('region.state.cities');

// TIPOS DE UNIDADAES
Route::get('/property-type','PropertyTypeController@index')->name('property_type.index');
Route::get('/property-type/edit/{id}','PropertyTypeController@edit')->name('property_type.edit');
Route::get('/property-type/delete/{id}','PropertyTypeController@delete')->name('property_type.delete');
Route::get('/property-type/create','PropertyTypeController@create')->name('property_type.create');
Route::post('/property-type/store','PropertyTypeController@store')->name('property_type.store');
Route::post('/property-type/update','PropertyTypeController@update')->name('property_type.update');

// UNIDADAES
Route::get('/property','PropertyController@index')->name('property.index');
Route::get('/property/edit/{id}','PropertyController@edit')->name('property.edit');
Route::get('/property/delete/{id}','PropertyController@delete')->name('property.delete');
Route::get('/property/create','PropertyController@create')->name('property.create');
Route::post('/property/store','PropertyController@store')->name('property.store');
Route::post('/property/update','PropertyController@update')->name('property.update');

// SERVIÇOS
Route::get('/service','ServiceController@index')->name('service.index');
Route::get('/service/edit/{id}','ServiceController@edit')->name('service.edit');
Route::get('/service/delete/{id}','ServiceController@delete')->name('service.delete');
Route::get('/service/create','ServiceController@create')->name('service.create');
Route::post('/service/store','ServiceController@store')->name('service.store');
Route::post('/service/update','ServiceController@update')->name('service.update');

// ORDEM DE SERVIÇO
Route::get('/os/{os_nro}','ServiceOrderController@index')->name('os.index');
// Route::get('/os/edit/{id}','ServiceOrderController@edit')->name('os.edit');
// Route::get('/os/delete/{id}','ServiceOrderController@delete')->name('os.delete');
Route::get('/service-order/create','ServiceOrderController@create')->name('os.create');
Route::post('/service-order/store','ServiceOrderController@store')->name('os.store');
Route::post('/service-order/update','ServiceOrderController@update')->name('os.update');
