<?php

Route::get('/carteiras', function () {
    return view('carteiras.index');
})->name('carteiras.index');

Route::get('/carteiras/show/{id}', function () {
    return view('carteiras.show');
})->name('carteiras.show');