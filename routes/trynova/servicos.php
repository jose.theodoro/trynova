<?php

Route::get('/servicos', function () {
    return view('servicos.index');
})->name('servicos.index');

Route::get('/servicos/show/{id}', function () {
    return view('servicos.show');
})->name('servicos.show');