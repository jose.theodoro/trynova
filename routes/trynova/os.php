<?php

Route::get('/os', function () {
    return view('os.index');
})->name('os.index');

Route::get('/os/show/{id}', function () {
    return view('os.show');
})->name('os.show');