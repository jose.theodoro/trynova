<?php

Route::get('/regioes', function () {
    return view('regioes.index');
})->name('regioes.index');

Route::get('/regioes/show/{id}', function () {
    return view('regioes.show');
})->name('regioes.show');