<?php

Route::get('/unidades', function () {
    return view('unidades.index');
})->name('unidades.index');

Route::get('/unidades/show/{id}', function () {
    return view('unidades.show');
})->name('unidades.show');